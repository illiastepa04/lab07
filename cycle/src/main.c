//Використовуємо заголовочний файл для роботи з printf
#include<stdio.h>
#include<main.h>
#include<stdbool.h>
//Використовуємо заголовочний файл для роботи з srand(time(NULL));
#include <time.h>



bool is_number_perfect(int number);



int main(){

	
	srand(time(NULL));
	
	//Створємо рандомні значення зміної х до 20
	int x = rand() % 20;

	if(is_number_perfect(x)){
		printf("Число досконале %d\n", x);
	} else{
		printf("Число не досконале %d\n", x);
	}
	
	return 0;
}

bool is_number_perfect(int number){
	
	int sum = 0;

	for(int i = 1; i < number; ++i){
		if(number % i == 0){
			sum += i;
		}
	}		

	if(sum == number){
		return true;
	}
	
	return false;
}
