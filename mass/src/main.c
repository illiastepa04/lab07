#include<stdio.h>
#include<time.h>

void string_aligning(char *consequence, int TEXT_LEN, char *diveder);

int main() {

	srand(time(NULL));

	char diveder = '_';

	int word_len = rand() % 10 + 1;

	int space_len = (rand() % 5) * 2 + 2;

	int TEXT_LEN = word_len + space_len;

	char consequence[TEXT_LEN];

	for(int i = 0; i < TEXT_LEN; i++){
		if(i < word_len){
			consequence[i] = rand() % 26 +'A';
		} else {
			consequence[i] = ' ';
		}
		printf("%c", consequence[i]);
	}
	printf("\n");

	string_aligning(consequence, TEXT_LEN, diveder);

	for(int i = 0; i < TEXT_LEN; i++){
		printf("%c", consequence[i]);
	}

	return 0;
}


void string_aligning(char *consequence, int TEXT_LEN, char *diveder){
	
	int space_count = 0;

	for(int i = 0; i < TEXT_LEN; i++){
		if(consequence[i] == ' '){
			space_count++;
		}	
	}

	int letters_count = TEXT_LEN - space_count - 1;

	for(; letters_count >= 0; letters_count--){
		consequence[letters_count+space_count/2] = consequence[letters_count];
	}

	for(int i = 0; i < space_count/2; i++){
		consequence[i] = diveder;
	}

	for(int i = TEXT_LEN-1; i >= TEXT_LEN - space_count/2; i--){
		consequence[i] = diveder;
	}
}

