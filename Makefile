CC = clang
C_OPTS = -g
clean:
	rm -rf dist
all:
	make clean prep compile
prep:
	mkdir dist
compile: main.bin

main.bin: src/main.c
	$(CC) $(C_OPTS) $< -o ./dist/$@

run: clean prep compile
	./dist/main.bin


# TODO: clang-tidy; scan-build
